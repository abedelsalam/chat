(function () {
    'use strict';
    angular
        .module('chatApp')
        .controller('LoginCtrl', function ($scope, $cookieStore, $state) {
            $scope.login = function () {
                $cookieStore.put('name', $scope.user);
                $state.go('chat');
            }
        });
})();