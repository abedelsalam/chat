'use strict';

var chatApp = angular.module('chatApp', ['ui.router', 'LocalStorageModule', 'ngCookies', 'mcwebb.sound']);


chatApp.config(['$stateProvider', '$cookiesProvider', '$urlRouterProvider', function ($stateProvider, $cookiesProvider, $urlRouterProvider) {
	$stateProvider
		.state('chat', {
			url: '/chat',
			views: {
				'appView': {
					templateUrl: 'chat/chat.html',
					controller: 'ChatCtrl',
				}
			}
		}).state('login', {
			url: '/login',
			views: {
				'appView': {
					templateUrl: 'login/login.html',
					controller: 'LoginCtrl',
				}
			}
		});
	$urlRouterProvider.otherwise('login');
}]);

chatApp.run(function ($rootScope, $state, $cookieStore, SoundService) {
	SoundService.loadSound({
		name: 'sound',
		src: 'sound.mp3'
	});

	$rootScope.user = $cookieStore.get('name');
	if (!$rootScope.user) {
		$state.go('login');
	} else {
		$state.go('chat');
	}
});
