(function () {
    'use strict';
    angular
        .module('chatApp')
        .controller('ChatCtrl', function ($scope, $cookieStore, $state, $timeout, SoundService) {
            var side = 'left';
            $scope.name = $cookieStore.get('name');
            $scope.now = getDate() + ' ' + getTime();
            $scope.randomMessages = [
                'Cottage out enabled was entered greatly prevent message.',
                'In to am attended desirous raptures declared diverted confined at',
                'Neat own nor she said see walk'
            ];

            $scope.messages = [];
            $scope.sendMessage = function () {
                $scope.messages.push({
                    avatar: 'person.png',
                    text: $scope.messageText,
                    name: $scope.name,
                    time: getTime(),
                    side: 'left'
                });

                $timeout(function () {
                    SoundService.getSound('sound').start();
                    
                    $scope.messages.push({
                        avatar: 'bot.png',
                        text: $scope.randomMessages[Math.floor((Math.random() * 2))],
                        name: 'Bot',
                        time: getTime(),
                        side: 'right'
                    });

                    
                }, 500);

                $scope.messageText = "";
            };

            $scope.logout = function () {
                $cookieStore.remove('logedin');
                $state.go('login');
            };
        });

    function AddZero(num) {
        return (num >= 0 && num < 10) ? "0" + num : num + "";
    }
    function getDate() {
        var now = new Date();
        var strDate = [[AddZero(now.getDate()),
        AddZero(now.getMonth() + 1),
        now.getFullYear()].join("/")].join(" ");

        return strDate;
    }
    function getTime() {
        var now = new Date();
        var strTime = [[AddZero(now.getHours()),
        AddZero(now.getMinutes())].join(":"),
        now.getHours() >= 12 ? "PM" : "AM"].join(" ");

        return strTime;
    }
})();